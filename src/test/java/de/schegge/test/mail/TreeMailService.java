package de.schegge.test.mail;


import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.BodyPart;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import java.io.File;
import java.util.Date;
import java.util.Optional;

class TreeMailService {

  private final Session session;

  TreeMailService(Session session) {
    this.session = session;
  }

  public void sendTreeMail(String... tos) throws MessagingException {
    sendTreeMail(null, tos);
  }

  public void sendTreeMail(File file, String... tos) throws MessagingException {
    sendTreeMail("This is message body", file, tos);
  }

  private void sendTreeMail(String content, File file, String... tos)
      throws MessagingException {
    session.setDebug(true);
    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress("noreply@schegge.de"));
    message.setRecipient(RecipientType.BCC, new InternetAddress("quality@schegge.de"));
    message.setRecipient(RecipientType.CC, new InternetAddress("stage@schegge.de"));
    message.setSentDate(new Date());
    message.setSubject("Test", "UTF-8");
    for (String to : tos) {
      message.addRecipient(RecipientType.TO, new InternetAddress(to));
    }
    Multipart multipart = new MimeMultipart();
    message.setContent(multipart);

    Optional.ofNullable(content).map(this::createText).ifPresent(a -> secureMessagingMethod(multipart::addBodyPart, a));
    Optional.ofNullable(file).map(this::createAttachment)
        .ifPresent(a -> secureMessagingMethod(multipart::addBodyPart, a));

    Transport.send(message);
  }

  private BodyPart createAttachment(File file) {
    try {
      BodyPart attachment = new MimeBodyPart();
      DataSource source = new FileDataSource(file);
      attachment.setDataHandler(new DataHandler(source));
      attachment.setFileName(file.getName());
      return attachment;
    } catch (MessagingException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private BodyPart createText(String content) {
    try {
      BodyPart text = new MimeBodyPart();
      text.setText(content);
      return text;
    } catch (MessagingException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  private interface MessagingConsumer<T> {

    void accept(T var1) throws MessagingException;
  }

  private <T> void secureMessagingMethod(MessagingConsumer<T> consumer, T value) {
    try {
      consumer.accept(value);
    } catch (MessagingException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
