package de.schegge.test.mail;

import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static de.schegge.test.mail.MailMatchers.hasCC;
import static de.schegge.test.mail.MailMatchers.hasFrom;
import static de.schegge.test.mail.MailMatchers.hasSubject;
import static de.schegge.test.mail.MailMatchers.hasTo;
import static de.schegge.test.mail.OptionalMatchers.hasValue;
import static de.schegge.test.mail.OptionalMatchers.isEmpty;
import static de.schegge.test.mail.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(GreenMailExtension.class)
@GreenMailSettings(setup = GreenMailSetup.SMTP)
class MailBoxTest {

    private static final File ATTACHMENT_FILE = new File("src/test/resources/ancestors.txt");

    private TreeMailService service;

    @BeforeEach
    void createService(Session mailSession) {
        service = new TreeMailService(mailSession);
    }

    @Test
    void testSmtpFindOne(GreenMailBox mailBox) throws MessagingException, ExecutionException, InterruptedException {
        service.sendTreeMail(ATTACHMENT_FILE, "importer@ahnen.de");

        Mail receivedMessage = mailBox.findOne().get();
        assertThat(receivedMessage.getSubject(), isPresent());
        assertThat(receivedMessage.getSubject(), not(isEmpty()));
        assertThat(receivedMessage.getSubject(), hasValue("Test"));
        assertThat(receivedMessage.getSubject(), hasValue(startsWith("Test")));

        assertEquals(List.of("stage@schegge.de"), receivedMessage.getCc());
        assertTrue(receivedMessage.getBcc().isEmpty());
        assertEquals(2, receivedMessage.getAllRecipients().size());
        assertNotNull(receivedMessage.getMessage());
    }

    @Test
    void testSmtpFindAllWithCount(GreenMailBox mailBox)
            throws ExecutionException, InterruptedException, MessagingException {
        service.sendTreeMail(ATTACHMENT_FILE, "info@ahnen.de");
        service.sendTreeMail(ATTACHMENT_FILE, "admin@vorfahren.org");

        List<Mail> receivedMessages = mailBox.findAll(2).get();
        assertThat(receivedMessages,
                hasItems(
                        allOf(
                                hasCC(hasItem("stage@schegge.de")),
                                hasTo(hasItem("info@ahnen.de")),
                                hasSubject(startsWith("Test"))),
                        allOf(hasTo(hasItem("admin@vorfahren.org")),
                                hasSubject(),
                                hasFrom(endsWith("schegge.de")))));
    }

    @Test
    void testWaitForMissingMail(GreenMailBox mailBox) {
        ExecutionException executionException = assertThrows(ExecutionException.class, () -> mailBox.findAll(1).get());
        assertEquals("no incoming mails!", executionException.getCause().getMessage());
    }
}
