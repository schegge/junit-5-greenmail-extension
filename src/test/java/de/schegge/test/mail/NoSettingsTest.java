package de.schegge.test.mail;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;

import java.util.concurrent.ExecutionException;

import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(GreenMailExtension.class)
class NoSettingsTest {

  @Test
  @Disabled
  void testSmtpFindOne(Session mailSession, GreenMailBox mailBox)
      throws MessagingException, ExecutionException, InterruptedException {
    new TreeMailService(mailSession).sendTreeMail("importer@ahnen.de");

    Mail receivedMessage = mailBox.findOne().get();
    assertThat(receivedMessage.getFrom(), endsWith("schegge.de"));
  }
}
