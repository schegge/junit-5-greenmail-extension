package de.schegge.test.mail;

import static org.junit.jupiter.api.extension.ExtensionContext.Namespace.create;
import static org.junit.platform.commons.support.AnnotationSupport.findAnnotation;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import jakarta.mail.Session;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;

public class GreenMailExtension implements BeforeEachCallback, AfterEachCallback, ParameterResolver {

  private static final Namespace GREENMAIL = create("de.schegge.greenmail");
  private static final String SERVER = "server";
  private static final String DEBUG = "debug";

  private static final List<Class<?>> SUPPORTED_PARAMETERS = List
      .of(GreenMailBox.class, Properties.class, Session.class);

  @Override
  public void beforeEach(ExtensionContext context) {
    Optional<GreenMailSettings> greenMailSettings = retrieveAnnotation(context);
    ServerSetup[] setups = greenMailSettings.map(GreenMailSettings::setup).stream().flatMap(Arrays::stream).distinct()
        .map(GreenMailSetup::getServerSetup).toArray(ServerSetup[]::new);
    boolean debug = greenMailSettings.map(GreenMailSettings::debug).orElse(false);

    GreenMail greenMail = new GreenMail(setups.length == 0 ? ServerSetupTest.ALL : setups);
    greenMail.start();
    context.getStore(GREENMAIL).put(SERVER, greenMail);
    context.getStore(GREENMAIL).put(DEBUG, debug);
  }

  @Override
  public void afterEach(ExtensionContext context) {
    context.getStore(GREENMAIL).get(SERVER, GreenMail.class).stop();
  }

  @Override
  public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
    return SUPPORTED_PARAMETERS.contains(parameterContext.getParameter().getType());
  }

  @Override
  public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
    Store store = extensionContext.getStore(GREENMAIL);
    GreenMail greenMail = store.get(SERVER, GreenMail.class);
    if (GreenMailBox.class == parameterContext.getParameter().getType()) {
      return new GreenMailBox(greenMail);
    }
    Properties properties = greenMail.getSmtp().getServerSetup()
        .configureJavaMailSessionProperties(new Properties(), store.get(DEBUG, boolean.class));
    if (Session.class == parameterContext.getParameter().getType()) {
      return Session.getDefaultInstance(properties);
    }
    return properties;
  }

  private Optional<GreenMailSettings> retrieveAnnotation(final ExtensionContext context) {
    ExtensionContext currentContext = context;
    do {
      Optional<GreenMailSettings> annotation = findAnnotation(currentContext.getElement(), GreenMailSettings.class);
      if (annotation.isPresent()) {
        return annotation;
      }
      currentContext = currentContext.getParent().orElse(context.getRoot());
    } while (currentContext != context.getRoot());
    return Optional.empty();
  }
}
