package de.schegge.test.mail;

import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;

public enum GreenMailSetup {
  SMTP(ServerSetupTest.SMTP),
  SMTPS(ServerSetupTest.SMTPS),
  POP3(ServerSetupTest.POP3),
  POP3S(ServerSetupTest.POP3S),
  IMAP(ServerSetupTest.IMAP),
  IMAPS(ServerSetupTest.IMAPS);

  private final ServerSetup serverSetup;

  GreenMailSetup(ServerSetup serverSetup) {
    this.serverSetup = serverSetup;
  }

  public ServerSetup getServerSetup() {
    return serverSetup;
  }
}
