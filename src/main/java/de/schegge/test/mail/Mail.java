package de.schegge.test.mail;

import jakarta.mail.Address;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class Mail {

  private final MimeMessage message;

  Mail(MimeMessage message) {
    this.message = Objects.requireNonNull(message);
  }

  public MimeMessage getMessage() {
    return message;
  }

  public Optional<String> getSubject() {
    return Optional.ofNullable(secureMessagingMethod(message::getSubject));
  }

  public String getFrom() {
    return stream(secureMessagingMethod(message::getFrom)).findFirst().orElseThrow(MailingException::new);
  }

  public List<String> getTo() {
    return recipients(RecipientType.TO).collect(toList());
  }

  public List<String> getCc() {
    return recipients(RecipientType.CC).collect(toList());
  }

  public List<String> getBcc() {
    return recipients(RecipientType.BCC).collect(toList());
  }

  public List<String> getAllRecipients() {
    return stream(secureMessagingMethod(message::getAllRecipients)).collect(toList());
  }

  private Stream<String> recipients(RecipientType type) {
    return stream(secureMessagingMethod(() -> message.getRecipients(type)));
  }

  private interface MessagingSupplier<T> {

    T get() throws MessagingException;
  }

  private <T> T secureMessagingMethod(MessagingSupplier<T> supplier) {
    try {
      return supplier.get();
    } catch (MessagingException e) {
      throw new MailingException(e);
    }
  }

  private Stream<String> stream(Address[] addresses) {
    if (addresses == null || addresses.length == 0) {
      return Stream.empty();
    }
    return Stream.of(addresses).map(Address::toString);
  }
}
