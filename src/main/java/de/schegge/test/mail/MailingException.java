package de.schegge.test.mail;

public class MailingException extends RuntimeException {

  public MailingException() {
  }

  public MailingException(Throwable cause) {
    super(cause);
  }
}
