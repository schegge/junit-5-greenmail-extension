package de.schegge.test.mail;

import java.util.Optional;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public final class OptionalMatchers {

  private static final String EMPTY = "empty";

  public static <T> Matcher<Optional<T>> isEmpty() {
    return new FeatureMatcher<>(CoreMatchers.is(true), EMPTY, EMPTY) {
      @Override
      protected Boolean featureValueOf(Optional<T> actual) {
        return actual.isEmpty();
      }
    };
  }

  public static <T> Matcher<Optional<T>> isPresent() {
    return new FeatureMatcher<>(CoreMatchers.is(true), EMPTY, EMPTY) {
      @Override
      protected Boolean featureValueOf(Optional<T> actual) {
        return actual.isPresent();
      }
    };
  }

  public static <T> Matcher<Optional<T>> hasValue(final T value) {
    return hasValue(CoreMatchers.equalTo(value));
  }

  public static <T> Matcher<Optional<T>> hasValue(final Matcher<T> matcher) {
    return new TypeSafeDiagnosingMatcher<>() {
      @Override
      public void describeTo(final Description description) {
        description.appendText("hasValue should return ").appendDescriptionOf(matcher);
      }

      @Override
      protected boolean matchesSafely(Optional<T> item, Description mismatchDescription) {
        if (item.isEmpty()) {
          mismatchDescription.appendText(" was empty");
          return false;
        }
        mismatchDescription.appendText(" was ").appendValue(item.get());
        T itemValue = item.get();
        return matcher.matches(itemValue);
      }
    };
  }
}
