package de.schegge.test.mail;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.junit.jupiter.api.extension.ExtendWith;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(GreenMailExtension.class)
@GreenMailSettings(setup = GreenMailSetup.SMTP, debug = true)
public @interface GreenMailSmtpTest {

}
