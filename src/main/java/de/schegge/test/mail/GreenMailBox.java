package de.schegge.test.mail;

import static java.util.stream.Collectors.toList;

import com.icegreen.greenmail.util.GreenMail;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class GreenMailBox {

  private final ExecutorService executor;
  private final GreenMail greenMail;

  GreenMailBox(GreenMail greenMail) {
    this.greenMail = greenMail;
    executor = Executors.newSingleThreadExecutor();
  }

  public CompletableFuture<Mail> findOne() {
    return findAll(1).thenApply(list -> list.get(0));
  }

  public CompletableFuture<List<Mail>> findAll() {
    return CompletableFuture
        .completedFuture(Stream.of(greenMail.getReceivedMessages()).map(Mail::new).collect(toList()));
  }

  public CompletableFuture<List<Mail>> findAll(int count) {
    CompletableFuture<List<Mail>> completableFuture = new CompletableFuture<>();
    executor.submit(() -> {
      if (!greenMail.waitForIncomingEmail(count)) {
        completableFuture.completeExceptionally(new IllegalArgumentException("no incoming mails!"));
        return null;
      }
      List<Mail> collect = Stream.of(greenMail.getReceivedMessages()).map(Mail::new).collect(toList());
      completableFuture.complete(collect);
      return null;
    });

    return completableFuture;
  }
}
