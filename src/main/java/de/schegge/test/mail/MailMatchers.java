package de.schegge.test.mail;

import java.util.Optional;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

public final class MailMatchers {

  public static Matcher<Mail> hasOptionalSubject(final Matcher<Optional<String>> subjectMatcher) {
    return new FeatureMatcher<>(subjectMatcher, "subject", "subject") {
      @Override
      protected Optional<String> featureValueOf(final Mail actual) {
        return actual.getSubject();
      }
    };
  }

  public static Matcher<Mail> hasSubject(final Matcher<String> subjectMatcher) {
    return hasOptionalSubject(OptionalMatchers.hasValue(subjectMatcher));
  }

  public static Matcher<Mail> hasSubject(final String subject) {
    return hasOptionalSubject(OptionalMatchers.hasValue(subject));
  }

  public static Matcher<Mail> hasSubject() {
    return hasOptionalSubject(OptionalMatchers.isPresent());
  }

  public static Matcher<Mail> hasFrom(final Matcher<String> fromMatcher) {
    return new FeatureMatcher<>(fromMatcher, "from", "from") {
      @Override
      protected String featureValueOf(final Mail actual) {
        return actual.getFrom();
      }
    };
  }

  public static Matcher<Mail> hasFrom(final String from) {
    return hasFrom(Matchers.equalTo(from));
  }

  public static Matcher<Mail> hasTo(final Matcher<Iterable<? super String>> toMatcher) {
    return new FeatureMatcher<>(toMatcher, "to", "to") {
      @Override
      protected Iterable<? super String> featureValueOf(final Mail actual) {
        return actual.getTo();
      }
    };
  }

  public static Matcher<Mail> hasCC(final Matcher<Iterable<? super String>> ccMatcher) {
    return new FeatureMatcher<>(ccMatcher, "cc", "cc") {
      @Override
      protected Iterable<? super String> featureValueOf(final Mail actual) {
        return actual.getCc();
      }
    };
  }
}
